package gotwilio

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

// ListResources contains Twilio paging information.
// See https://www.twilio.com/docs/usage/twilios-response#response-formats-list-paging-information
type ListResources struct {
	Uri             string `json:"uri"`
	FirstPageUri    string `json:"first_page_uri"`
	NextPageUri     string `json:"next_page_uri"`
	PreviousPageUri string `json:"previous_page_uri"`
	Page            uint   `json:"page"`
	PageSize        uint   `json:"page_size"`

	Faxes    []*FaxResource `json:"faxes"`
	Messages []*SmsResponse `json:"messages"`

	t *Twilio
}

func (t *Twilio) newListResources() *ListResources {
	lr := new(ListResources)
	lr.t = t
	return lr
}

func (l *ListResources) hasNext() bool {
	return l.NextPageUri != ""
}

func (l *ListResources) next() (*ListResources, *Exception, error) {
	if !strings.Contains(l.NextPageUri, "https://api.twilio.com") {
		l.NextPageUri = "https://api.twilio.com" + l.NextPageUri
	}
	resp, err := l.t.get(l.NextPageUri)
	if err != nil {
		return nil, nil, err
	}

	l.NextPageUri = "" // Reset next page just incase unmarshall doesn't update
	l.Messages = make([]*SmsResponse, 0)
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}
	if resp.StatusCode != http.StatusOK {
		exc := new(Exception)
		err = json.Unmarshal(respBody, exc)
		return nil, exc, err
	}

	newList := l.t.newListResources()
	json.Unmarshal(respBody, newList)
	return newList, nil, json.Unmarshal(respBody, l)
}
